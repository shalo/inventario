<!doctype html>
<hmtl>
	<head>
		<title>resguardo</title>
		<link rel="stylesheet" type="text/css" href="stylesres.css">
		<link href="css/bootsrap.min.css" rel="stylesheet">
		<link href="css/bootsrap-theme.css" rel="stylesheet">
		<script src="js/jquery-3.4.1.min.js"></script>
		<script src="js/bootsrap.min.js"></script>
		<meta charset="UTF-8">
	</head>


	<body>
		<div class="container">
			<div class="row">
				<h3 style="text-align:center">NUEVO REGISTRO</h3>
			</div>
			
			<form class="form-horizontal" method="POST" action="guardar6.php" autocomplete="off">
				<div class="form-group">
					<label for="nombre" class="col-sm-2 control-label">Nombre</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese el nombre del empleado" required>
					</div>
				</div>

				<div class="form-group">
					<label for="nombre" class="col-sm-2 control-label">Sucursal</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese el nombre de la sucursal" required>
					</div>
				</div>
								
				
				<div class="form-group">
					<label for="equipo_tipo" class="col-sm-2 control-label">Seleciona equipo</label>
					<div class="col-sm-10">
						<select class="form-control" id="estado_civil" name="estado_civil">
							<option value="SOLTERO">LapTop</option>
							<option value="CASADO">Teclado</option>
							<option value="OTRO">Monitor</option>
						</select>
					</div>
				</div>

				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<a href="index.php" class="btn btn-default">Regresar</a>
						<button type="submit" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>